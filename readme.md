[_Read the english version here..._](https://gitlab.com/rauldipeas/debian-gamer/-/blob/main/english-readme.md)

Este projeto visa facilitar a preparação do [**Debian** `bullseye`](https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/weekly-live-builds/amd64/iso-hybrid) para a finalidade de jogos.

Neste contexto, você vai encontrar comandos para a instalação do kernel [**XanMod**](https://xanmod.org), que traz mais desempenho de forma geral pro sistema, além de várias ferramentas para a instalação de jogos, como a [**Steam**](https://store.steampowered.com), o [**Heroic Games Launcher**](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher) ([_Epic Game Store_](https://www.epicgames.com)), a [**Minigalaxy**](https://sharkwouter.github.io/minigalaxy/) ([_GOG Galaxy_](https://www.gog.com/)), o [**itch**](https://itch.io), o [**Lutris**](https://lutris.net), o [**WINE Launcher**](http://wine.hostronavt.ru) e o [**GameHub**](https://tkashkin.tk/projects/gamehub/).

Além também da instalação do [**WINE TkG**](https://github.com/Kron4ek/Wine-Builds) e do [**Proton GE**](https://github.com/AUNaseef/protonup), com suporte ao **FSYNC**, que garante o máximo de performance aos jogos.

Se você precisar de ajuda, tiver uma sugestão ou só quiser trocar experiências, pode acessar o [**Discord**](https://discord.gg/bEVNHfg) do projeto.

<a href=https://discord.gg/bEVNHfg><img src="https://upload.wikimedia.org/wikipedia/commons/7/7a/Newdiscordlogo.png" width="50%" height="50%"></a>

Para executar todo o conteúdo dessa página de forma automatizada, basta rodar o comando a seguir no terminal:

`bash <(curl -s https://gitlab.com/rauldipeas/debian-gamer/-/raw/main/debian-gamer.sh)`

Status da execução dos comandos: [![pipeline](https://gitlab.com/rauldipeas/debian-gamer/badges/main/pipeline.svg)](https://gitlab.com/rauldipeas/debian-gamer/-/pipelines)


# ![apt](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/distributor-logo-debian.svg) [Correção do apt (apt fix)](https://wiki.debian.org/AptConfiguration)
```sh
sudo cp -v /etc/apt/sources.list /etc/apt/sources.list.BKP
cat /etc/apt/sources.list|grep -v debian-security|grep -v deb-src|sudo tee /etc/apt/sources.list
sudo sed -i 's/main/main contrib non-free/g' /etc/apt/sources.list
cat <<EOF |sudo tee -a /etc/apt/sources.list
# Sid
deb https://deb.debian.org/debian sid main contrib non-free
EOF
cat <<EOF |sudo tee /etc/apt/preferences.d/sid
# Sid
Package: *
Pin: release a=unstable
Pin-Priority: 100
EOF
sudo apt update
```

# ![system-tools](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/utilities-tweak-tool.svg) Ferramentas do sistema (system tools)
```sh
xdg-open https://aria2.github.io
xdg-open https://github.com/scop/bash-completion
xdg-open https://www.gnu.org/software/wget
sudo apt install -y aria2 bash-completion wget
```
# ![xanmod](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/org.gnome.Firmware.svg) [XanMod](https://xanmod.org)
```sh
xdg-open https://xanmod.org
echo 'deb http://deb.xanmod.org releases main'|sudo tee /etc/apt/sources.list.d/xanmod-kernel.list
curl -s https://dl.xanmod.org/gpg.key|sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/xanmod-kernel.gpg
sudo apt update
sudo apt install -y linux-xanmod
```

# ![lutris](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/lutris.svg) [Lutris](https://lutris.net)
```sh
xdg-open https://lutris.net
echo 'deb http://download.opensuse.org/repositories/home:/strycore/Debian_10/ ./'|sudo tee /etc/apt/sources.list.d/lutris.list
curl -s https://download.opensuse.org/repositories/home:/strycore/Debian_10/Release.key|sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/lutris.gpg
sudo dpkg --add-architecture i386
sudo apt update
sudo apt install -y libvulkan1:i386 libpng16-16:i386
sudo apt install -y --no-install-recommends lutris
```

# ![wine](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/wine.svg) [WINE-TkG](https://github.com/Kron4ek/Wine-Builds)
```sh
xdg-open https://github.com/Kron4ek/Wine-Builds
aria2c $(curl -s https://api.github.com/repos/Kron4ek/Wine-Builds/releases|grep browser_download_url|grep staging-tkg-amd64.tar.xz|head -n1|cut -d '"' -f4)
tar -xvf wine*staging-tkg-amd64.tar.xz
sudo mv -v wine*staging-tkg-amd64 /opt/wine-tkg
aria2c $(curl -s https://api.github.com/repos/GloriousEggroll/wine-ge-custom/releases|grep browser_download_url|grep download|grep .tar.xz|head -n1|cut -d '"' -f4)
tar -xvf wine-lutris-ge*.tar.xz
sudo cp -v lutris*/lib/wine/i386-unix/winemenubuilder.exe.so /opt/wine-tkg/lib/wine/i386-unix/winemenubuilder.exe.so
sudo cp -v lutris*/lib/wine/i386-windows/winemenubuilder.exe /opt/wine-tkg/lib/wine/i386-windows/winemenubuilder.exe
sudo cp -v lutris*/lib64/wine/x86_64-unix/winemenubuilder.exe.so /opt/wine-tkg/lib/wine/x86_64-unix/winemenubuilder.exe.so
sudo cp -v lutris*/lib64/wine/x86_64-windows/winemenubuilder.exe /opt/wine-tkg/lib/wine/x86_64-windows/winemenubuilder.exe
#WINE_GECKO_VER=$(curl -s https://dl.winehq.org/wine/wine-gecko/|grep folder|cut -d '"' -f6|sort -g|tail -n1)
WINE_GECKO_VER=2.47.2/
curl -s https://dl.winehq.org/wine/wine-gecko/$WINE_GECKO_VER|grep x86|grep tar|grep -wv pdb|cut -d '"' -f6>wine-gecko.links
sed -i 's/wine-gecko/https:\/\/dl.winehq.org\/wine\/wine-gecko\/wine-gecko/g' wine-gecko.links
sed -i 's@wine\/wine-gecko\/@'wine\/wine-gecko\/"$WINE_GECKO_VER"'@g' wine-gecko.links
aria2c `echo $(cat wine-gecko.links|head -n1)`
aria2c `echo $(cat wine-gecko.links|tail -n1)`
WINE_MONO_VER=$(curl -s https://dl.winehq.org/wine/wine-mono/|grep folder|cut -d '"' -f6|sort -g|tail -n1)
curl -s https://dl.winehq.org/wine/wine-mono/$WINE_MONO_VER|grep x86|grep tar|cut -d '"' -f6>wine-mono.links
sed -i 's/wine-mono/https:\/\/dl.winehq.org\/wine\/wine-mono\/wine-mono/g' wine-mono.links
sed -i 's@wine\/wine-mono\/@'wine\/wine-mono\/"$WINE_MONO_VER"'@g' wine-mono.links
aria2c `echo $(cat wine-mono.links|head -n1)`
sudo mkdir -pv /opt/wine-tkg/share/wine/{gecko,mono}
sudo tar xvf wine-gecko-*-x86.tar.xz -C /opt/wine-tkg/share/wine/gecko/
sudo tar xvf wine-gecko-*-x86_64.tar.xz -C /opt/wine-tkg/share/wine/gecko/
sudo tar xvf wine-mono-*-x86.tar.xz -C /opt/wine-tkg/share/wine/mono/
sudo mkdir -pv /etc/X11/Xsession.d
cat <<EOF |sudo tee /etc/X11/Xsession.d/99wine
export WINEFSYNC=1
if [ -d "/opt/wine-tkg/bin" ] ; then
    PATH="/opt/wine-tkg/bin:\$PATH"
fi
EOF
```

# ![q4wine](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/q4wine.svg) [Q4Wine](https://q4wine.brezblock.org.ua)
```sh
xdg-open https://q4wine.brezblock.org.ua
sudo apt install -y --no-install-recommends q4wine
```

# ![cpu-x](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/cpu-x.svg) [CPU-X](https://x0rg.github.io/CPU-X)
```sh
xdg-open https://x0rg.github.io/CPU-X
sudo apt install -y cpu-x
```

# ![gamemode](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/preferences-desktop-gaming.svg) [GameMode](https://github.com/FeralInteractive/gamemode)
```sh
xdg-open https://github.com/FeralInteractive/gamemode
sudo apt install -y --no-install-recommends gamemode
```

# ![mangohud](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/meterbridge.svg) [MangoHud](https://github.com/flightlessmango/MangoHud)
```sh
xdg-open https://github.com/flightlessmango/MangoHud
sudo apt install -y --no-install-recommends mangohud
```

# ![vkbasalt](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/colorpicker.svg) [vkBasalt](https://github.com/DadSchoorse/vkBasalt)
```sh
xdg-open https://github.com/DadSchoorse/vkBasalt
sudo apt install -y --no-install-recommends vkbasalt
```

# ![goverlay](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/goverlay.svg) [GOverlay](https://github.com/benjamimgois/goverlay)
```sh
xdg-open https://github.com/benjamimgois/goverlay
sudo apt install -y goverlay
```

# ![heroic](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/heroic.svg) [Heroic](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher)
```sh
xdg-open https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher
aria2c $(curl -s https://api.github.com/repos/Heroic-Games-Launcher/HeroicGamesLauncher/releases|grep browser_download_url|grep amd64.deb|head -n1|cut -d '"' -f4)
sudo apt install -y ./heroic*.deb
```

# ![steam](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/steam.svg) [Steam](https://store.steampowered.com)
```sh
xdg-open https://store.steampowered.com
aria2c https://cdn.cloudflare.steamstatic.com/client/installer/steam.deb
sudo apt install -y libgl1-mesa-dri:i386 libgl1:i386 ./steam*.deb
```

# ![proton](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/wine.svg) [Proton GE](https://github.com/AUNaseef/protonup)
```sh
xdg-open https://github.com/AUNaseef/protonup
sudo apt install -y python3-setuptools
git clone https://github.com/AUNaseef/protonup
cd protonup
python3 setup.py install --user
cat <<EOF | tee -a ~/.bashrc
# Proton GE
if [ -d "\$HOME/.local/bin" ] ; then
    PATH="\$HOME/.local/bin:\$PATH"
fi
EOF
source ~/.bashrc
yes|$HOME/.local/bin/protonup
cd
```

# ![protontricks](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/wine.svg) [Protontricks](https://github.com/Matoking/protontricks)
```sh
xdg-open https://github.com/Matoking/protontricks
sudo apt install -y python3-pip
git clone https://github.com/Matoking/protontricks
cd protontricks
sudo python3 setup.py install
cd
```

# ![itch](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/itch.svg) [itch](https://itch.io)
```sh
xdg-open https://itch.io
aria2c https://itch.io/app/download?platform=linux
chmod +x -v itch-setup
./itch-setup --silent
```

# ![minigalaxy](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/minigalaxy.svg) [Minigalaxy](https://sharkwouter.github.io/minigalaxy)
```sh
xdg-open https://sharkwouter.github.io/minigalaxy
aria2c $(curl -s https://api.github.com/repos/sharkwouter/minigalaxy/releases|grep browser_download_url|grep all.deb|head -n1|cut -d '"' -f4)
sudo apt install -y ./minigalaxy*.deb
```

# ![gamehub](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/com.github.tkashkin.gamehub.svg) [GameHub](https://tkashkin.tk/projects/gamehub/)
```sh
xdg-open https://tkashkin.tk/projects/gamehub/
aria2c $(curl -s https://api.github.com/repos/tkashkin/GameHub/releases|grep browser_download_url|grep amd64.deb|head -n1|cut -d '"' -f4)
sudo apt install -y ./GameHub*deb
```

# ![wine-launcher](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/wine-launcher.svg) [WINE Launcher](http://wine.hostronavt.ru)
```sh
xdg-open http://wine.hostronavt.ru
mkdir -pv $HOME/.local/share/wine-launcher $HOME/.local/share/applications
cd $HOME/.local/share/wine-launcher
curl -L https://github.com/hitman249/wine-launcher/releases/latest/download/start --output start
chmod +x -v start
./start
cat <<EOF |tee ~/.local/share/applications/wine-launcher.desktop
[Desktop Entry]
Type=Application
Name=WINE Launcher
Exec=$HOME/.local/share/wine-launcher/bin/start
Icon=wine-launcher
Categories=Game;
EOF
mkdir -pv ~/.local/share/icons
curl -sL https://github.com/hitman249/wine-launcher/raw/master/build/icons/512.png --output $HOME/.local/share/icons/wine-launcher.png
cd
```

# ![discord](https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/32x32/apps/discord.svg) [Discord](https://discord.com)
```sh
xdg-open https://discord.com
aria2c -o discord.deb 'https://discord.com/api/download?platform=linux&format=deb'
sudo apt install -y ./discord*.deb
```
