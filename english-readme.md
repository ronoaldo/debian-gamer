This project aims to facilitate the preparation of the [**Debian** `bullseye`](https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/weekly-live-builds/amd64/iso-hybrid) for gaming purposes.

In this context, you will find commands for installing the [**XanMod**](https://xanmod.org) kernel, which brings more overall performance to the system, as well as several tools for installing games, such as the [**Steam**](https://store.steampowered.com), the [**Heroic Games Launcher**](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher) ([_Epic Game Store_](https://www.epicgames.com)), the [**Minigalaxy**](https://sharkwouter.github.io/minigalaxy) ([_GOG Galaxy_](https://www.gog.com)), the [**itch**](https://itch.io), the [**Lutris**](https://lutris.net), the [**WINE Launcher**](http://wine.hostronavt.ru) and the [**GameHub**](https://tkashkin.tk/projects/gamehub/).

Besides also installing [**WINE TkG**](https://github.com/Kron4ek/Wine-Builds) and [**Proton GE**](https://github.com/AUNaseef/protonup ), with **FSYNC** support, which guarantees maximum gaming performance.

If you need help, have a suggestion or just want to exchange experiences, you can access the [**Discord**](https://discord.gg/bEVNHfg) of the project.

<a href=https://discord.gg/bEVNHfg><img src="https://upload.wikimedia.org/wikipedia/commons/7/7a/Newdiscordlogo.png" width="50%" height=" 50%"></a>

To run the entire content of _main readme_ page in an automated way, just run the following command in the terminal:

`bash <(curl -s https://gitlab.com/rauldipeas/debian-gamer/-/raw/main/debian-gamer.sh)`

Command execution status: [![pipeline](https://gitlab.com/rauldipeas/debian-gamer/badges/main/pipeline.svg)](https://gitlab.com/rauldipeas/debian-gamer/-/pipelines)


[_Back to main readme with the scripts..._](https://gitlab.com/rauldipeas/debian-gamer/-/blob/main/readme.md)
