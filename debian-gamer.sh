#!/bin/bash
set -e
echo -e "#/bin/bash\nset -e"|tee debian-gamer.sh
curl -s https://gitlab.com/rauldipeas/debian-gamer/-/raw/main/readme.md|awk '/```sh/{flag=1; next}/```/{flag=0} flag'|tee -a debian-gamer.sh
bash -x debian-gamer.sh
